import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html',
  styleUrls: ['./fetch-data.component.css']
})
export class FetchDataComponent implements OnInit {

  constructor(private http: HttpClient) { }
  //Country
  getCountryList(): Observable<any[]> {
    return this.http.get<any>(environment.baseUrl + 'api/country');
  }

  addCountry(data: any) {
    return this.http.post(environment.baseUrl + 'api/country',data);
  }

  updateCountry(id: number | string, data: any) {
    return this.http.put(environment.baseUrl + `api/country/${id}`,data);
  }
  deleteCountry(id: number | string) {
    return this.http.delete(environment.baseUrl + `api/country/${id}`);
  }

  //City

  getCityList(): Observable<any[]> {
    return this.http.get<any>(environment.baseUrl + 'api/city');
  }

  addCity(data: any) {
    return this.http.post(environment.baseUrl + 'api/city', data);
  }

  updateCity(id: number | string, data: any) {
    return this.http.put(environment.baseUrl + `api/city/${id}`, data);
  }
  deleteCity(id: number | string) {
    return this.http.delete(environment.baseUrl + `api/city/${id}`);
  }

  ngOnInit(): void {
  }

}
