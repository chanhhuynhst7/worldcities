import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule ,ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { WorldcitiesComponent } from './worldcities/worldcities.component';
import { ShowWorldcitiesComponent } from './worldcities/show-worldcities/show-worldcities.component';
import { AddEditWorldcitiesComponent } from './worldcities/add-edit-worldcities/add-edit-worldcities.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';

@NgModule({
  declarations: [
    AppComponent,
    WorldcitiesComponent,
    ShowWorldcitiesComponent,
    AddEditWorldcitiesComponent,
    FetchDataComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [FetchDataComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
