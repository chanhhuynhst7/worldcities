import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowWorldcitiesComponent } from './show-worldcities.component';

describe('ShowWorldcitiesComponent', () => {
  let component: ShowWorldcitiesComponent;
  let fixture: ComponentFixture<ShowWorldcitiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowWorldcitiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowWorldcitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
