import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { FetchDataComponent } from 'src/app/fetch-data/fetch-data.component';
@Component({
  selector: 'app-show-worldcities',
  templateUrl: './show-worldcities.component.html',
  styleUrls: ['./show-worldcities.component.css']
})
export class ShowWorldcitiesComponent implements OnInit {

  cityList$!: Observable<any[]>;
  countryList$!: Observable<any[]>;
  countryList: any = [];

  //Map to display data associate with foreign keys

  countryMap: Map<number, string> = new Map();

  constructor(private fetch:FetchDataComponent) { }

  ngOnInit(): void {
    this.cityList$ = this.fetch.getCityList();
    this.countryList$ = this.fetch.getCountryList();
    this.refreshCountryMap();
  }

  //Variables (properties)

  modalTitle: string = '';
  activateAddEditWorldcitiesComponent: boolean = false;
  city : any;


  modalAdd() {
    this.city = {
      id: 0,
      cityName: null,
      countryId: null
    }
    this.modalTitle = "Add World Cities"
    this.activateAddEditWorldcitiesComponent = true;
  }

  modalEdit(item: any) {
    this.city = item;
    this.modalTitle = "Edit City";
    this.activateAddEditWorldcitiesComponent = true;
  }

  delete(item: any) {
    if (confirm(`Are you sure you want to delete city ${item.id}`)) {
      this.fetch.deleteCity(item.id).subscribe(() => {
        var closeModalBtn = document.getElementById('add-edit-modal-close');
        if (closeModalBtn) {
          closeModalBtn.click();
        }

        var showDeleteSuccess = document.getElementById('delete-success-alert');
        if (showDeleteSuccess) {
          showDeleteSuccess.style.display = "block";
        }
        setTimeout(function () {
          if (showDeleteSuccess) {
            showDeleteSuccess.style.display = "none"
          }
        }, 400);
        this.cityList$ = this.fetch.getCityList();
      })
    }
  }
  modalClose() {
    this.activateAddEditWorldcitiesComponent = false;
    this.cityList$ = this.fetch.getCityList();
  }

  refreshCountryMap() {
    this.fetch.getCountryList().subscribe(data => {
      this.countryList = data;
      for (let i = 0; i < data.length; i++) {
        this.countryMap.set(this.countryList[i].id,this.countryList[i].countryName);
      }
    })
  }
}
