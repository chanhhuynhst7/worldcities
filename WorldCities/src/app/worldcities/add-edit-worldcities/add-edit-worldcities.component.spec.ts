import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditWorldcitiesComponent } from './add-edit-worldcities.component';

describe('AddEditWorldcitiesComponent', () => {
  let component: AddEditWorldcitiesComponent;
  let fixture: ComponentFixture<AddEditWorldcitiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditWorldcitiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditWorldcitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
