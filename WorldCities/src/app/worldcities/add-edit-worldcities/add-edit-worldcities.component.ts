import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { FetchDataComponent } from 'src/app/fetch-data/fetch-data.component';

@Component({
  selector: 'app-add-edit-worldcities',
  templateUrl: './add-edit-worldcities.component.html',
  styleUrls: ['./add-edit-worldcities.component.css']
})
export class AddEditWorldcitiesComponent implements OnInit {
  cityList$!: Observable<any[]>;
  countryList$!: Observable<any[]>;
  constructor(private fetch: FetchDataComponent) { }


  @Input() city: any;
  id: number = 0;
  cityName: string = "";
  countryId!: number;


  ngOnInit(): void {
    this.id = this.city.id;
    this.cityName = this.city.cityName;
    this.countryId = this.city.countryId;
    this.cityList$ = this.fetch.getCityList();
    this.countryList$ = this.fetch.getCountryList();
  }
  addCity() {
    var city = {
      cityName: this.cityName,
      countryID: this.countryId

    }
    this.fetch.addCity(city).subscribe(res => {
      var closeModalBtn = document.getElementById('add-edit-modal-close');
      if (closeModalBtn) {
        closeModalBtn.click();
      }

      var showAddSuccess = document.getElementById('add-success-alert');
      if (showAddSuccess) {
        showAddSuccess.style.display = "block";
      }
      setTimeout(function () {
        if (showAddSuccess) {
          showAddSuccess.style.display = "none"
        }
      }, 400);
    })
  }
  updateCity() {
    var city = {
      id: this.id,
      cityName: this.cityName,
      countryID: this.countryId

    }
    var id: number = this.id;
    this.fetch.updateCity(id, city).subscribe(res => {
      var closeModalBtn = document.getElementById('add-edit-modal-close');
      if (closeModalBtn) {
        closeModalBtn.click();
      }

      var showUpdateSuccess = document.getElementById('update-success-alert');
      if (showUpdateSuccess) {
        showUpdateSuccess.style.display = "block";
      }
      setTimeout(function () {
        if (showUpdateSuccess) {
          showUpdateSuccess.style.display = "none"
        }
      }, 4000);
    })
  }

}
