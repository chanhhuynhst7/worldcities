﻿using System.ComponentModel.DataAnnotations;

namespace WorldCitiesAPI
{
    public class City
    {
        public int Id { get; set; }

        [StringLength(50)] 
        public string CityName { get; set; }


        public int CountryId { get; set; }

        public Country? Country { get; set; }   
    }
}
