﻿using Microsoft.EntityFrameworkCore;

namespace WorldCitiesAPI.Data
{
    public class DataContext :DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) :base(options){ }


        public DbSet<Country> Countries { get; set; }

        public DbSet<City> Cities { get; set; } 
    }
}
