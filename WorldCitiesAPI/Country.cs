﻿using System.ComponentModel.DataAnnotations;

namespace WorldCitiesAPI
{
    public class Country
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string CountryName { get; set; }
    }
}
